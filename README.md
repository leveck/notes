# notes

```
NOTE by Nathaniel Leveck 2018
Released to the public domain

BEFORE first use:
	notes -setup (this creates the data directory ~/.notes and the notes file)
USAGE:
	[options CANNOT be stacked!]
	notes -h || --help (this usage message)
	notes -a "Text to add"
	notes [-l] (lists noncompleted notes, this is the default action)
	notes -la (lists all notes)
	notes -lc (lists completed notes)
	notes -d # (deletes note #)
	notes -x # (toggle note # completed flag)
	notes -t "search term" (search noncompleted notes)
	notes -ta "search term" (search all notes)
	notes -tc "search term" (search completed notes)
	notes -al "file.name" "text to display" (add linked long form note, must be file in ~/.notes)
	notes -ll # (cat linked note)
```